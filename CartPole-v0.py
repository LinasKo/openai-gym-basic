import gym
import numpy as np
from time import sleep

np.random.seed(743)
env = gym.make('CartPole-v0')
print("Actions:", env.action_space)
print("Observations", env.observation_space, (env.observation_space.high, env.observation_space.low))


def try_passing(parameters):
	print("Attempting to pass with parameters:", parameters)
	result_list = []
	average_res = 0
	for _ in range(100):
		reward = run_episode(parameters)
		result_list.append(reward)
		average_res = sum(result_list) / 100
	return average_res > 195.0, average_res


def run_episode(parameters, sleep_time=0.0):
	observation = env.reset()
	total_reward = 0
	for _ in range(200):
		env.render()
		sleep(sleep_time)

		action = 0
		if parameters.dot(observation) > 0:
			action = 1

		observation, reward, done, info = env.step(action)
		total_reward += reward

		if done:
			break

	return total_reward

res_list = []
for episode in range(100):
	params = np.random.rand(4) * 2 - 1
	print("Trying params:", params)

	ep_reward = run_episode(params)

	if ep_reward >= 195:
		success, average_reward = try_passing(params)

		if success:
			print("Finished!")
			print("Episodes for training:", episode)
			print("Parameters:", params)
			break
